﻿using Dapper;
using PretestCoreSilmi.Models;
using PretestCoreSilmi.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Data;


namespace PretestCoreSilmi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyController : ControllerBase
    {
        private readonly IJWTAuthManager _authentication;
        public CompanyController(IJWTAuthManager authentication)
        {
            _authentication = authentication;
        }

       
        [Microsoft.AspNetCore.Mvc.HttpGet("CompanyList")]

        [Authorize(Roles = "Admin")]
        public IActionResult getCompany()
        {
            var result = _authentication.getCompanyList<ModelCompany>();

            return Ok(result);
        }

        [Microsoft.AspNetCore.Mvc.HttpPost("Create")]
        [AllowAnonymous]
       // [Authorize(Roles = "Admin")]
        public IActionResult Register([System.Web.Http.FromBody] ModelCompany Company)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("name", Company.Name, DbType.String);
            dp_param.Add("address", Company.Address, DbType.String);
            dp_param.Add("email", Company.Email, DbType.String);
            dp_param.Add("telephone", Company.Telephone, DbType.String);
            dp_param.Add("flag", Company.Flag, DbType.String);
            dp_param.Add("iduser", Company.CreatedBy, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelCompany>("sp_createCompany", dp_param);
            if (result.Code == 200)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }

        [Microsoft.AspNetCore.Mvc.HttpPut("Update")]
        [Authorize(Roles = "Admin")]
        public IActionResult Update([System.Web.Http.FromBody] ModelCompany Company, string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("name", Company.Name, DbType.String);
            dp_param.Add("address", Company.Address, DbType.String);
            dp_param.Add("email", Company.Email, DbType.String);
            dp_param.Add("telephone", Company.Telephone, DbType.String);
            dp_param.Add("flag", Company.Flag, DbType.String);
            dp_param.Add("iduser", Company.CreatedBy, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelCompany>("sp_updateCompany", dp_param);
            if (result.Code == 200)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }

        [Microsoft.AspNetCore.Mvc.HttpDelete("Delete")]
        [Authorize(Roles = "Admin")]
        public IActionResult Delete(string id)
        {
            if (id == string.Empty)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelCompany>("sp_deleteCompany", dp_param);

            if (result.Code == 200)
            {
                return Ok(result);
            }

            return NotFound(result);
        }
    }
}