﻿using Dapper;
using PretestCoreSilmi.Models;
using PretestCoreSilmi.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Data;


namespace PretestCoreSilmi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentController : ControllerBase
    {
        private readonly IJWTAuthManager _authentication;
        public DocumentController(IJWTAuthManager authentication)
        {
            _authentication = authentication;
        }

       
        [Microsoft.AspNetCore.Mvc.HttpGet("DocumentList")]

        [Authorize(Roles = "Admin")]
        public IActionResult getDocument()
        {
            var result = _authentication.getDocumentList<ModelDocument>();

            return Ok(result);
        }

        [Microsoft.AspNetCore.Mvc.HttpPost("Create")]
        [AllowAnonymous]
        //[Authorize(Roles = "Admin")]
        public IActionResult Register([System.Web.Http.FromBody] ModelDocument Document)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("idcompany", Document.IDCompany, DbType.Int32);
            dp_param.Add("idCategory", Document.IDCategory, DbType.Int32);
            dp_param.Add("name", Document.Name, DbType.String);
            dp_param.Add("description", Document.Description, DbType.String);
            dp_param.Add("flag", Document.Flag, DbType.String);
            dp_param.Add("iduser", Document.CreatedBy, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelDocument>("sp_createDocument", dp_param);
            if (result.Code == 200)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }

        [Microsoft.AspNetCore.Mvc.HttpPut("Update")]
        [Authorize(Roles = "Admin")]
        public IActionResult Update([System.Web.Http.FromBody] ModelDocument Document, string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.Int32);
            dp_param.Add("idcompany", Document.IDCompany, DbType.Int32);
            dp_param.Add("idcategory", Document.IDCategory, DbType.Int32);
            dp_param.Add("name", Document.Name, DbType.String);
            dp_param.Add("description", Document.Description, DbType.String);
            dp_param.Add("flag", Document.Flag, DbType.String);
            dp_param.Add("iduser", Document.CreatedBy, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelDocument>("sp_updateDocument", dp_param);
            if (result.Code == 200)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }

        [Microsoft.AspNetCore.Mvc.HttpDelete("Delete")]
        [Authorize(Roles = "Admin")]
        public IActionResult Delete(string id)
        {
            if (id == string.Empty)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelDocument>("sp_deleteDocument", dp_param);

            if (result.Code == 200)
            {
                return Ok(result);
            }

            return NotFound(result);
        }
    }
}