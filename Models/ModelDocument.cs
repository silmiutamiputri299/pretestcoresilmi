﻿using System.ComponentModel.DataAnnotations;

namespace PretestCoreSilmi.Models
{
    public class ModelDocument
    {
        [Required]
        public int? IDCompany { get; set; }

        [Required]
        public int? IDCategory { get; set; }

        [Required]
        public string? Name { get; set; }

        [Required]
        public string? Description { get; set; }

        [Required]
        public string? Flag { get; set; }


        [Required]
        public string? CreatedBy { get; set; }


        [Required]
        public DateTime Date { get; set; } = DateTime.Now;
    }
}
